function GetArray2D(row = 1, col = 1) {
    var result = [];
    for (let r = 0; r < row; r++) {
        var resRow = [];
        for (let c = 0; c < col; c++) {
            resRow.push('');
        }
        result.push(resRow);
    }
    return result;
}

function print(arr) {
    var cnt = '<table border = "1">';
    for (let row = 0; row < arr.length; row++) {
        cnt += '<tr>';
        var cols = arr[row];
        for (let col = 0; col < cols.length; col++) {
            cnt += `<td text-align: center>${arr[row][col]}</td>`;
        }
        cnt += '</tr>';
    }
    cnt += '</table>';
    return cnt;
}

function fibonaci(n = 1) {
    var result = [];
    for (let i = 0; i < n; i++) {
        if (i < 2) {
            result.push(1);
        } else {
            result.push(result[i - 1] + result[i - 2]);
        }
    }
    return result;
}

function tribonaci(n = 1) {
    var result = [];
    for (let i = 0; i < n; i++) {
        if (i < 3) {
            result.push(1);
        } else {
            result.push(result[i - 1] + result[i - 2] + result[i - 3]);
        }
    }
    return result;
}

function prima(n = 1) {
    var a, b, c, d;
    a = 2;
    b = 3;
    c = 5;
    d = 7;
    var j = 1;
    var nilai = [];

    for (i = 1; i < 1000; i++) {
        if (i == 1) {
            i += 0;
        } else if (i == a || i == b || i == c || i == d) {
            nilai.push(i);
        } else if (i % a == 0 || i % b == 0 || i % c == 0 || i % d == 0) {
            i += 0;
        } else {
            nilai.push(i);
        }
    }
    return nilai;
}