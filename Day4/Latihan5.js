function Exam0415(kata = "aku") {
    var arr2d = GetArray2D(1, 1);
    var berubah = kata.split('');
    var panjang = (berubah.length + (berubah.length + 1));
    var j = 0;

    for (let i = 0; i < panjang; i++) {
        if (i == 0) {
            arr2d[0][0] = berubah.join('');
        } else if (i == 1) {
            arr2d[0][1] = "=";
        } else if (i > 1 && i % 2 == 0) {
            arr2d[0][i] = berubah[j]
            j++;
        } else if (i > 2 && i % 2 == 1) {
            arr2d[0][i] = "+";
        }
    }
    return print(arr2d);
}