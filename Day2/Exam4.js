function Exam0204(n = 1){
    var arr2d = GetArray2D(2, n);
    var b = 0;
    var c = 0;
    for(let row = 0; row < 2; row++){
        for(let col = 0; col < n; col++){
            if(row == 0){
                arr2d[row][col] =  col;
            }else if(row == 1 && col % 2 == 0 ){
                    arr2d[row][col] = b = b+1;
            }else if(row == 1 && col % 1 == 0 ){
                    arr2d[row][col] =  c = c+5;
            }
        }
    }
    return print(arr2d);
}