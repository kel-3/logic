function Exam0201(n = 1){
    var arr2d = GetArray2D(2, n);
    for(let row = 0; row < 2; row++){
        for(let col = 0; col < n; col++){
            if(row == 0){
                arr2d[row][col] =  col;
            }else{
                arr2d[row][col] = Math.pow(3,col);
            }
        }
    }
    return print(arr2d);
}