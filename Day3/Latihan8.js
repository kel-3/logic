function Exam0318(n = 1){
    var arr2d = GetArray2D(1,n);
    var charA = 65;

    for(let h = 0; h < n; h++){
        if(h % 2 == 0){
            arr2d[0][h] = String.fromCharCode(charA+h);
        }else{
            arr2d[0][h] = h+1;
        }
        
    }
    return print(arr2d);
}