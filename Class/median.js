class median {
    constructor(arr = [1]) {
        this.list = arr; //JSON.parse(arr);
        this.arr2d = GetArray2D(1, this.list.length);

        this.FillArray();
    }

    FillArray() {
        const arr = this.arr2d[0];
        //menuangkan nilai list kedalam array
        for (let idx = 0; idx < arr.length; idx++) {
            arr[idx] = this.list[idx];
        }

        //Lokasi sorting
        while (true) {
            //membandingkan dan memindahkan 
            for (let idx = 1; idx < arr.length; idx++) {
                if (arr[idx - 1] > arr[idx]) {
                    var temp = arr[idx - 1];
                    arr[idx - 1] = arr[idx];
                    arr[idx] = temp;
                }
            }
            //testing sort
            var sorted = true;
            for (let idx = 1; idx < arr.length; idx++) {
                if (arr[idx - 1] > arr[idx]) {
                    sorted = false;
                }
            }
            if (sorted) {
                break;
            }
        }

    }
    Print() {
        return print(this.arr2d);
    }
    Printmed() {
        //MEDIAN
        var hasil = GetArray2D(6, 2);
        if (this.list.length % 2 == 1) {
            var m = (this.list.length + 1) / 2;
            hasil[0][0] = "MEDIAN"
            hasil[0][1] = this.arr2d[0][m - 1];
        } else {
            var m = ((this.list.length) / 2);
            hasil[0][0] = "MEDIAN"
            hasil[0][1] = (this.arr2d[0][m] + this.arr2d[0][m - 1]) / 2;
        }

        //MEAN
        var sum = 0;
        for (let idx = 0; idx < this.list.length; idx++) {
            sum = this.arr2d[0][idx] + sum;
        }
        hasil[1][0] = "MEAN"
        hasil[1][1] = sum / this.list.length;

        //MIN
        hasil[2][0] = "MIN"
        var min;
        for (let idx = 0; idx < this.list.length; idx++) {
            if (idx == 0) {
                min = this.arr2d[0][idx];
            } else if (this.arr2d[0][idx] < min) {
                min = this.arr2d[0][idx];
            }
        }
        hasil[2][1] = min;

        //MAX
        hasil[3][0] = "MAX"
        var max;
        for (let idx = 0; idx < this.list.length; idx++) {
            if (idx == 0) {
                max = this.arr2d[0][idx];
            } else if (this.arr2d[0][idx] > max) {
                max = this.arr2d[0][idx];
            }
        }
        hasil[3][1] = max;

        //MODUS
        hasil[4][0] = "MODUS"
        var a = [];
        var max = 0;
        var nilai = 0;

        for (let idx = 0; idx < this.list.length - 1; idx++) {
            var sum = 0;

            for (let a = 0; a < this.list.length; a++) {
                if (this.arr2d[0][idx] == this.arr2d[0][a]) {
                    sum++;
                }
                if (sum > max) {
                    max = sum;
                    nilai = this.arr2d[0][idx];
                }
            }
            
        }

        


        hasil[4][1] = nilai;

        //URUT
        hasil[5][0] = "HASIL SORTING"
        hasil[5][1] = this.arr2d;


        return print(hasil);
    }


}